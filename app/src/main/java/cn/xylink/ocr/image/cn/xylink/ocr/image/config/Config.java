package cn.xylink.ocr.image.cn.xylink.ocr.image.config;

/**
 * 描述：系统常量配置
 */
public class Config {

    /**
     * 全局Handler消息状态码 10000-20000
     */
    public static final int MESSAGE_TEST = 10001;
    public static final int MESSAGE_NEWMSG_BADAGE = 10002;
    public static final int MESSAGE_IMAGE_CAMERA = 10003;
    public static final int MESSAGE_IMAGE_SWITCHER = 10004;
    public static final int MESSAGE_IMAGE_UPLOAD = 10006;
    public static final int TO_SELECT_PHOTO = 10005;
    public static final int MESSAGE_MAP_POINT = 10006;
    public static final int MESSAGE_MAP_TRACE = 10007;
    public static final int MESSAGE_LOCATION = 10008;

    public static final String APP_DIR_NAME =  "/Android/data/com.blog.hechaojie.android";
    public static final String TOKEN_FILE = "token_file";
    public static final String SYSTEM_TOKEN = "_token_";
    public static final String DEVICEID = "_deviceid_";
    public static final String TIMESTAMP = "_timestamp_";
    public static final String USERID = "_USERID_";
    public static final String HX_PASSWD = "_HX_PASSWD_";

    public static final Long COMMON_CACHE_5M = 5 * 60L;
    public static final Long COMMON_CACHE_2M = 2 * 60L;
    public static final Long COMMON_NO_CACHE = -1L;


}