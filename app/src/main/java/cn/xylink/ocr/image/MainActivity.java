package cn.xylink.ocr.image;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.Map;
import cn.xylink.ocr.image.cn.xylink.ocr.image.config.Config;
import cn.xylink.ocr.image.cn.xylink.ocr.image.config.OcrKit;
import cn.xylink.ocr.image.cn.xylink.ocr.image.config.OpenCvMananager;
import cn.xylink.ocr.image.view.SelectActivity;

public class MainActivity extends Activity {

    ImageView imageView ;
    ImageView target ;
    TextView text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        imageView = (ImageView) this.findViewById(R.id.image);
        target = (ImageView) this.findViewById(R.id.target);
        text = (TextView) this.findViewById(R.id.text);
        this.findViewById(R.id.btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                click(v);
            }
        });
        this.findViewById(R.id.select).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                click(v);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        OpenCvMananager.init();
    }

    private void click(View v) {
        if(v.getId() == R.id.btn){
            try {
                ocr();
            }catch (Exception ex){
                ex.printStackTrace();
            }
        } else if(v.getId() == R.id.select){
            try {
//                Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.src_image);
//                imageView.setImageBitmap(bitmap);
                // 选择照片
                Intent intent = new Intent(this, SelectActivity.class);
                intent.putExtra("type","SELECT");
                startActivityForResult(intent, Config.TO_SELECT_PHOTO);
            }catch (Exception ex){
                Log.e("tess",ex.getMessage());
                ex.printStackTrace();
            }
        }
    }
    /**
     * 识别
     */
    private void ocr (){
        String resultText = "";
        try {
            text.setText("识别中...");
            Bitmap bitmap =((BitmapDrawable) ((ImageView) imageView).getDrawable()).getBitmap();
            Map<String,Object> result = OcrKit.getOCR(this,bitmap);
            Bitmap targetBitMap = (Bitmap) result.get("targetBitMap");
            resultText = (String) result.get("resultText");
            target.setImageBitmap(targetBitMap);
            Toast.makeText(this, "识别结果:" + resultText, Toast.LENGTH_LONG).show();
        }catch (Exception ex){
            Log.e("tess",ex.getMessage());
            ex.printStackTrace();
        }finally {
            text.setText("识别结果:"+resultText);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try{
            if(data == null){
                return;
            }
            Uri mImageCaptureUri = data.getData();
            if (mImageCaptureUri != null) {
                Bitmap photoBmp = MediaStore.Images.Media.getBitmap(this.getContentResolver(), mImageCaptureUri);
                imageView.setImageBitmap(photoBmp);
            }
        } catch (Exception ex){
            ex.printStackTrace();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

}
