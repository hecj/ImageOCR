package cn.xylink.ocr.image.cn.xylink.ocr.image.config;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import com.googlecode.tesseract.android.TessBaseAPI;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import cn.xylink.ocr.image.R;

/**
 * @author hecj
 */
public class TessBaseAPIFactory {

    static TessBaseAPI tessApi;

    static final String DEFAULT_LANGUAGE = "xylinkfont";

    public static TessBaseAPI getTessBaseAPIInstance(Context context){
        try {

//            String datapath= Environment.getExternalStorageDirectory().getAbsolutePath()+ "/tesseract";
            String datapath = context.getExternalCacheDir().getAbsoluteFile()+"/tesseract";
//            String datapath = Environment.getExternalStoragePublicDirectory("").getAbsolutePath()+File.separator+"tesseract";
            Log.i("tess","datapath:"+datapath);

            File datapathFile = new File(datapath);
            if(!datapathFile.exists()){
                datapathFile.mkdirs();
            }

            tessApi = new TessBaseAPI();
            File dir = new File(datapath);
            tessApi.setDebug(true);
            if (!dir.exists()) {
                dir.mkdirs();
            }
            File tessdatadir = new File(datapath+File.separator+"tessdata");
            if(!tessdatadir.exists()){
                tessdatadir.mkdirs();
            }

            File file = new File(dir.getAbsolutePath()+File.separator+"tessdata"+File.separator+"xylinkfont.traineddata");
            if(!file.exists()){
                file.createNewFile();
            }
            Log.i("tess","file:"+file.getAbsolutePath());
            FileOutputStream output = new FileOutputStream(file);
            byte[] buff = new byte[1024];
            int len = 0;
            InputStream input = context.getResources().openRawResource(R.raw.xylinkfont);
            while ((len = input.read(buff)) != -1) {
                output.write(buff, 0, len);
            }
            input.close();
            output.close();
            boolean success = tessApi.init(datapath, DEFAULT_LANGUAGE);
            if (success) {
                Log.i("tess", "load Tesseract OCR Engine successfully...");
            } else {
                Log.i("tess", "WARNING:could not initialize Tesseract data...");
            }
        } catch (Exception ex){
            ex.printStackTrace();
        }
        return tessApi;
    }

}
