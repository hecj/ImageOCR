package cn.xylink.ocr.image.cn.xylink.ocr.image.config;

import android.content.Context;
import android.graphics.Bitmap;

import com.googlecode.tesseract.android.TessBaseAPI;

import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.util.HashMap;
import java.util.Map;

/**
 * @author hecj
 * 识别
 */
public class OcrKit {

    /**
     * 图像识别
     * @param context
     * @param bitmap
     * @return
     */
    public static Map<String,Object> getOCR(Context context,Bitmap bitmap){
        Map<String,Object> result = new HashMap<String, Object>();
        try {
            Mat src = new Mat();
            Utils.bitmapToMat(bitmap,src);

            Mat dst = new Mat();
            // 大小
            int newRows = 300;
            int newCols = (int)((double)newRows/src.size().width * src.size().height);
            Imgproc.resize(src, dst, new Size(newRows, newCols));

            // 灰度
            Mat hui = new Mat();
            Imgproc.cvtColor(dst, hui, Imgproc.COLOR_BGR2GRAY);
            // 二值
            Mat erzhi = new Mat();
            Imgproc.threshold(hui, erzhi, 0, 255, Imgproc.THRESH_BINARY | Imgproc.THRESH_OTSU);// 灰度图像二值化
            // 去噪声
//            Mat zao = new Mat();
//            Imgproc.medianBlur(erzhi, zao, 7);

            Bitmap newBitMap = Bitmap.createBitmap(erzhi.width(),erzhi.height(),Bitmap.Config.ARGB_8888);

            Utils.matToBitmap(erzhi,newBitMap);

            TessBaseAPI tessApi = TessBaseAPIFactory.getTessBaseAPIInstance(context);
            tessApi.setImage(newBitMap);
            // 获取返回值
            String resultText = tessApi.getUTF8Text();
            tessApi.end();

            // 释放
            src.release();
            dst.release();
            hui.release();
            erzhi.release();
//            zao.release();

            result.put("targetBitMap",newBitMap);
            result.put("resultText",resultText);

        } catch (Exception ex){
            ex.printStackTrace();
        }
        return result;

    }

}
